# Comment marche le projet Gitlab?

(ReadMe servant de test au 1e commit sur ce projet)

GitLab est un logiciel opensource de gestion de projet et �galement est un h�bergeur opensource de code versionn� gr�ce � git.


# Serveur et h�bergeur de code:
En travaillant sur un projet on est ammen� � modifier notre code, revenir sur le m�me code plusieurs fois, voir vouloir retrouver du code supprim�. 
D'autant qu'en travaillant � plusieurs on a besoin du code de chacun des contributeurs. Chacun peut modifier le code de l'autre. Il y a un risque de modification/suppression non d�sir�. Mais aussi de mutualisation du code.

Git et Gitlab permet de r�pondre � ses probl�mes, on mutualise le code sur un d�p�t en ligne dans gitlab, mais on peut sauvegarder des verions du code (1.0, 1.5, etc) que l'on peut retrouver par la suite. 
Git est un logiciel de versionnage d�centralis�. Chaque utilisateur poss�de une copie du code en local sur son pc et peut acc�der au serveur git pour prendre (pull) ou contribuer (commit et puch) au projet. 



# Gestion de projet
Les "features" qui nous int�resse sont assez simples � prendre en main:
- les issues, qui permet de d�couper les �volution, d�veloppement et bugs en diff�rentes t�ches. Ch�que t�che peut avoir une description, des pi�ces jointes (diagramme, figure, photo...) et aussi les possibles commentaires de chacun des contributeurs. Ce qui permet de suivre son �volution tout en �tant rattach� � la t�che.

- le issue board, un tableau type "kanban" qui regroupe le travaille futur, en cours et pass� sur les t�ches.

- le wiki : permet de faire des pages tutos ou info sur diff�rents topics.

etc...