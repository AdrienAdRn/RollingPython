from Image import*
import logging
import cv2
from matplotlib import pyplot as plt
import os

# set level of logging to info
logging.basicConfig(filename='test_Image.log', level=logging.INFO, format='%(asctime)s;%(levelname)s;%(message)s')

# !!!!! Besoin de setup et teardown pour les differents pas de test
height: int = 180
width: int = 240


# basic test without setup teardown practice

def test_get_height():
    logging.info("test : test_get_height")
    img_test = Image(height, width)
    result: int = img_test.get_height()
    #logging.debug("result of the test is " + "{}".format(result == 180))
    assert result == height



def test_set_height():
    logging.info("test : test_set_height")
    val_for_test: int = 45
    img_test = Image(height, width)
    img_test.set_height(val_for_test)
    assert img_test.get_height() == val_for_test



def test_get_width():
    logging.info("test : test_get_width")
    img_test = Image(height, width)
    result: int = img_test.get_width()
    assert result == width


def test_set_width():
    logging.info("test : test_set_width")
    img_test = Image(height, width)
    val_for_test: int = 45
    img_test.set_width(val_for_test)
    assert img_test.width == val_for_test




def test_set_data_from_file():
    logging.info("test_set_data_from_file")
    source_img_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input"
    logging.info("moving to the directory {}".format(source_img_path))
    os.chdir(source_img_path)
    img_ref = cv2.imread('TotoImage.jpg',1)
    img_test = Image(height, width)
    img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input", 'TotoImage.jpg')
    assert img_test.get_data().all() == img_ref.all()

