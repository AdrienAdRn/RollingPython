from Geometrical_Transform_2D_lib import*
from GrayImage import*
import math
import numpy as np
import cv2
import pytest
import logging


# set level of logging to info
logging.basicConfig(filename='test_Image.log', level=logging.INFO, format='%(asctime)s;%(levelname)s;%(message)s')

@pytest.mark.mirror
def test_mirror_image():
    logging.info("test : test_mirror_image")
    mirror_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\2D_geometrical_transformation_output"
    current_path = os.getcwd()

    # test img
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    direction_title_array = ["vertically","horizontally"]
    for direction in [1,2]:

        direction_title = direction_title_array[direction - 1]
        # mirror img_test vertically
        logging.info("test : mirror img_test {}".format(direction_title))
        mirrored_data = mirror_image(gray_img_test, direction)

        #display the result
        plt.figure()
        plt.title('image mirrored {}'.format(direction_title))
        plt.imshow(mirrored_data, cmap="gray")
        os.chdir(mirror_output_path)
        plt.savefig('image mirrored {}'.format(direction_title))
    plt.show()




@pytest.mark.rotation
def test_rotation():
    logging.info("test : test_rotates the_image")
    # test img
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

    rotation_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\2D_geometrical_transformation_output"
    current_path = os.getcwd()

    rotation_title_array = ["left","right"]
    counter = 0
    for direction in [-1,1]:

        direction_title = rotation_title_array[counter]
        # rotates img_test by 90 degrees
        logging.info("test : rotates img_test by 90 degrees to the {}".format(direction_title))
        rotated_data = rotation(gray_img_test, direction)

        #display the result
        plt.figure()
        plt.title('image rotated to the {}'.format(direction_title))
        plt.imshow(rotated_data, cmap="gray")
        os.chdir(rotation_output_path)
        plt.savefig('image rotated to the {}'.format(direction_title))
        counter +=1
    plt.show()



@pytest.mark.rotation
def test_theta_rotation():
    logging.info("test : test_rotates the_image by an angle theta")
    # test img
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')
    theta_rotation_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\2D_geometrical_transformation_output"
    current_path = os.getcwd()

    # rotates img_test by 20 degrees with given order
    theta_array = [-20.0, 20]
    order = 1
    for counter in [0, 1]:

        theta = theta_array[counter]
        logging.info("test : rotates img_test by {} degrees, order interpolation is {}".format(theta, order))
        theta_rotated_data = theta_rotation(gray_img_test, theta, order)

        #save and display the result
        plt.figure()
        plt.title('rotation with order {}, theta = {}°'.format(order, theta))
        plt.imshow(theta_rotated_data, cmap="gray")

        os.chdir(theta_rotation_output_path)
        theta_int = int(theta)  #savefig does not allow float variables in string formatting
        plt.savefig('rotation with order {}, theta = {}°'.format(order, theta_int))
        os.chdir(current_path)
        
    plt.show()


@pytest.mark.rotation_issue
def test_theta_rotation_order_interpolation_issue():
    logging.info("test : test_rotates the_image multiple times by an angle theta")
    # test img
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    theta_rotation_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\2D_geometrical_transformation_output"
    current_path = os.getcwd()

    # rotates img_test 9 times by 45 degrees with order 0

    nb_rotation = 9.0
    theta = float(360.0/nb_rotation)
    order = 1
    for counter in range(int(nb_rotation)):
        logging.info("test : rotates img_test by {} degrees, order interpolation is {}".format(theta, order))
        theta_rotated_data = theta_rotation(gray_img_test, theta, order)
        gray_img_test.set_data(theta_rotated_data)

        print('rotation n°{}'.format(counter+1))

    # save and display the result
    plt.figure()
    plt.title('consequences of {} rotation with order {}'.format(int(nb_rotation), order))
    plt.imshow(theta_rotated_data, cmap="gray")

    os.chdir(theta_rotation_output_path)
    plt.savefig('consequences of {} rotation with order {}'.format(int(nb_rotation), order))
    os.chdir(current_path)
    plt.show()
