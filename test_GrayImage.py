from GrayImage import*
import logging
import cv2
import pytest
from matplotlib import pyplot as plt
import os

# set level of logging to info
logging.basicConfig(filename='test_GrayImage.log', level=logging.INFO, format='%(asctime)s;%(levelname)s;%(message)s')

# !!!!! Besoin de setup et teardown pour les differents pas de test
height: int = 180
width: int = 240

# input img path independent from the machine
current_path = os.getcwd()
input_img_folder = os.path.join(current_path, "images_test_input")
toto_img_path = os.path.join(input_img_folder, "TotoImage" + "." + "jpg")


# basic test without setup teardown practice

@pytest.mark.getter
def test_get_height():
    logging.info("test : test_get_height for gray image class")
    gray_img_test = GrayImage(height, width)
    result: int = gray_img_test.get_height()

    assert result == height


@pytest.mark.setter
def test_set_height():
    logging.info("test : test_set_height for gray image class")
    val_for_test: int = 45
    gray_img_test = GrayImage(height, width)
    gray_img_test.set_height(val_for_test)
    assert gray_img_test.get_height() == val_for_test


@pytest.mark.getter
def test_get_width():
    logging.info("test : test_get_width for gray image class")
    gray_img_test = GrayImage(height, width)
    result: int = gray_img_test.get_width()
    assert result == width

@pytest.mark.setter
def test_set_width():
    logging.info("test : test_set_width for gray image class")
    gray_img_test = GrayImage(height, width)
    val_for_test: int = 45
    gray_img_test.set_width(val_for_test)
    assert gray_img_test.width == val_for_test



@pytest.mark.setter
def test_set_data_from_file():
    logging.info("test_set_data_from_file for gray image class")
    logging.info("moving to the directory {}".format(input_img_folder))
    os.chdir(input_img_folder)
    gray_img_ref = cv2.imread('TotoImage.jpg',0)
    gray_img_test = GrayImage(height, width)
    #gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input", 'TotoImage.jpg')
    gray_img_test.set_data_from_file(input_img_folder, 'TotoImage.jpg')
    assert gray_img_test.get_data().all() == gray_img_ref.all()

