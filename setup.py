# setup.py

"""Setup for Simple Lib"""
#from simple_lib import __version__
import GrayImage
import Geometrical_Transform_2D_lib

from setuptools import (
    setup,
    find_packages
)

setup(
    name='RollingPython',
    version= '0.0.5',
    long_description='Python library for Proof of Concept',
    author='Adrien GRZESIAK',
    packages=find_packages(
        exclude=[
            "*.tests",
            "*.tests.*",
            "tests.*",
            "tests",
            "log",
            "log.*",
            "*.log",
            "*.log.*"
        ]
    ),
    install_requires=[
        'docker==2.5.1',
    ]
)