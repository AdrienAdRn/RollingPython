from BGR_Geometrical_Transform_2DLib import*
from Image import*
import math
import numpy as np
import cv2
import logging


# set level of logging to info
logging.basicConfig(filename='test_Image.log', level=logging.INFO, format='%(asctime)s;%(levelname)s;%(message)s')



logging.info("test : test_mirror_image")
# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# mirror img_test vertically
logging.info("test : mirror img_test vertically")
direction: int = 1
BGR_mirror_image(img_test, direction)

#display the result
img_test.mapp_to_uint8()
img_test.display_img_data('mirrored_image')





logging.info("test : test_mirror_image")
# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# mirror img_test vertically
logging.info("test : mirror img_test horizontally")
direction: int = -1
BGR_mirror_image(img_test, direction)

#display the result
img_test.mapp_to_uint8()
img_test.display_img_data('mirrored_image')




logging.info("test : test_rotates the_image")
# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# rotates img_test by 90 degrees to the right
logging.info("test : rotates img_test by 90 degrees to the right")
direction: int = 1
BGR_rotation(img_test, direction)

#display the result
img_test.mapp_to_uint8()
img_test.display_img_data('image rotated to the right')


# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# rotates img_test by 90 degrees to the left
logging.info("test : rotates img_test by 90 degrees to the left")
direction: int = -1
BGR_rotation(img_test, direction)

#display the result
img_test.mapp_to_uint8()
img_test.display_img_data('image rotated to the left')



logging.info("test : test_rotates the_image by an angle theta")
# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# rotates img_test by 20 degrees to the right
theta: float = 20
order = 0
logging.info("test : rotates img_test by {} degrees to the right, order interpolation is {}".format(theta, order))
BGR_theta_rotation(img_test, theta, order)

#display the result
img_test.mapp_to_uint8()
img_test.display_img_data('image rotated to the right')



logging.info("test : test_rotates the_image by an angle theta")
# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# rotates img_test by -50 degrees to the right
theta: float = -50
order = 0
logging.info("test : rotates img_test by {} degrees to the left, order interpolation is {}".format(theta, order))
BGR_theta_rotation(img_test, theta, order)

#display the result
img_test.mapp_to_uint8()
img_test.display_img_data('image rotated to the left')


logging.info("test : test_rotates the_image by an angle theta")
# test img
img_test = Image(720, 1280)
img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

# rotates img_test by 45 degrees to the right
theta: float = 45
order = 1
logging.info("test : rotates img_test by {} degrees to the right, order interpolation is {}".format(theta, order))
BGR_theta_rotation(img_test, theta, order)

#display the result
#img_test.mapp_to_uint8()
img_test.display_img_data('image rotated to the right')