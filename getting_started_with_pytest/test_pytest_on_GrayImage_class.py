import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from GrayImage import*
import logging
import cv2
import pytest
from matplotlib import pyplot as plt
#import os

# set level of logging to info
logging.basicConfig(filename='test_GrayImage.log', level=logging.INFO, format='%(asctime)s;%(levelname)s;%(message)s')

# !!!!! Besoin de setup et teardown pour les differents pas de test
height: int = 180
width: int = 240


# setup et teardown functions
@pytest.fixture
def gray_img_test():
    gray_img = GrayImage(height, width)
    return gray_img




@pytest.mark.getter
def test_get_height(gray_img_test):
    logging.info("test : test_get_height for gray image class")
    #gray_img_test = GrayImage(height, width)
    result: int = gray_img_test.get_height()
    assert result == height


""""
@pytest.mark.setter
def test_set_height():
    logging.info("test : test_set_height for gray image class")
    val_for_test: int = 45
    gray_img_test = GrayImage(height, width)
    gray_img_test.set_height(val_for_test)
    assert gray_img_test.get_height() == val_for_test


@pytest.mark.getter
def test_get_width():
    logging.info("test : test_get_width for gray image class")
    gray_img_test = GrayImage(height, width)
    result: int = gray_img_test.get_width()
    assert result == width

@pytest.mark.setter
def test_set_width():
    logging.info("test : test_set_width for gray image class")
    gray_img_test = GrayImage(height, width)
    val_for_test: int = 45
    gray_img_test.set_width(val_for_test)
    assert gray_img_test.width == val_for_test



@pytest.mark.setter
def test_set_data_from_file():
    logging.info("test_set_data_from_file for gray image class")
    source_img_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input"
    logging.info("moving to the directory {}".format(source_img_path))
    os.chdir(source_img_path)
    gray_img_ref = cv2.imread('TotoImage.jpg',0)
    gray_img_test = GrayImage(height, width)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input", 'TotoImage.jpg')
    assert gray_img_test.get_data().all() == gray_img_ref.all()
"""

""""
def setup_module(module):
    #global height
    #  global width
    height: int = 180
    width: int = 240
    gray_img_test = GrayImage(height, width)

def teardown_module(module):
    gray_img_test = None
""""
