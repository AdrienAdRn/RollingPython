import toto_calc_lib as tcl
import pytest

image_format = "RGB"

def test_calc_sum():
    a = 5
    b = 10
    total = a + b
    assert total == tcl.calc_sum(a,b)

def test_calc_product():
    a: float = 6.0
    b: float = 3.0
    total: float = a*b
    assert total == tcl.calc_product(a,b)


@pytest.mark.skip(reason = "la raison du pourquoi")
def test_function_to_skip():
    a: float = 6.0
    b: float = 3.0
    total: float = a * b
    assert total == tcl.calc_product(a, b)

@pytest.mark.skipif(image_format == "RGB", reason = "format of the image is RGB")
def test_function_to_skipif():
    a: float = 6.0
    b: float = 3.0
    total: float = a * b
    assert total == tcl.calc_product(a, b)

@pytest.mark.windows
def test_windows_1():
    assert True

@pytest.mark.windows
def test_windows_2():
    assert True


@pytest.mark.mac
def test_mac_1():
    assert True


@pytest.mark.mac
def test_mac_2():
    assert True
