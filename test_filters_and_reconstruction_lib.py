from filters_and_reconstruction_lib import*
from GrayImage import*
import logging
import pytest
from matplotlib import pyplot as plt

# set level of logging to info
logging.basicConfig(filename='test_Image.log', level=logging.INFO, format='%(asctime)s;%(levelname)s;%(message)s')


@pytest.mark.low_pass_filter
def test_average_blur():
    logging.info("test : test_grayscale_image")
    # test img
    gray_img_test = GrayImage(720, 1280)
    blurred_img = GrayImage(720,1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

    # blur the image
    logging.info("test : blur the image with an average/mean filter")
    current_path: str = os.getcwd()
    average_blur_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\low_pass_filter_output"

    for order in [3, 5, 7, 9, 11, 13, 15, 17, 19, 21]:

        blurred_data = average_blur(gray_img_test, order)

        #display the result
        plt.figure()
        plt.subplot(2,1,1)
        plt.imshow(gray_img_test.get_data(), cmap=plt.get_cmap('gray'))
        plt.title('original image')
        plt.subplot(2,1,2)
        plt.imshow(blurred_data, cmap=plt.get_cmap('gray'))
        plt.title('blurred image with average blur of order {}'.format(order))

        # save the output
        os.chdir(average_blur_path)
        plt.savefig('average filter of order {}'.format(order))
        os.chdir(current_path)

    plt.show()


@pytest.mark.low_pass_filter
def test_median_filer():
    # test median filter
    logging.info("test : median filter")
    gray_img_test = GrayImage(720, 1280)
    filtered_img = GrayImage(720,1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

    filtered_data = filtered_img.get_data()
    neighborhood_size = 3
    filtered_data = median_filter(gray_img_test, neighborhood_size)
    filtered_img.set_data(filtered_data)

    #display the result
    plt.subplot(2,1,1)
    plt.imshow(gray_img_test.get_data(), cmap=plt.get_cmap('gray'))
    plt.title('original image')
    plt.subplot(2,1,2)
    plt.imshow(filtered_data, cmap=plt.get_cmap('gray'))
    plt.title('median filtered image of size {}'.format(neighborhood_size))
    plt.show()


    # save the output
    current_path: str = os.getcwd()
    low_pass_filter_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\low_pass_filter_output"
    os.chdir(low_pass_filter_path)
    cv2.imwrite("median filter output with n 3.jpg", filtered_data)
    os.chdir(current_path)


@pytest.mark.low_pass_filter
def test_bilinear_filter():
    logging.info("test : bilinear approximation of gaussian filter")
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')
    bilinear_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\low_pass_filter_output"
    current_path = os.getcwd()


    for order in [3,5]:
        filtered_data = bilinear_filter(gray_img_test, order)

        #display the result
        plt.figure()
        plt.subplot(2,1,1)
        plt.title('original image')
        plt.imshow(gray_img_test.get_data(), cmap=plt.get_cmap('gray'))
        plt.subplot(2,1,2)
        plt.title('gaussian filter of order {}'.format(order))
        plt.imshow(filtered_data, cmap=plt.get_cmap('gray'))
        os.chdir(bilinear_output_path)
        plt.savefig('gaussian filter of order {}'.format(order))
        os.chdir(current_path)
    plt.show()


"""""
logging.info("test : test_2D_convolution")
# test convolve
gray_img_test = GrayImage(720, 1280)
blurred_img = GrayImage(720,1280)
gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')

Id_kernel = np.matrix([[0, 0, 0],
                       [0, 1, 0],
                       [0, 0, 0]])

result = convolve(gray_img_test, Id_kernel)

assert (result == gray_img_test.get_data())
"""


# test prewitt_x
@pytest.mark.edge_detection_perso_function
def test_func_perso_prewitt():
    logging.info("test: prewitt_x function")
    gray_img_test = GrayImage(180,240)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",'TotoImage.jpg')
    filtered_img = GrayImage(180,240)
    filtered_data = prewitt_x(gray_img_test)
    filtered_img.set_data(filtered_data)
    gray_img_test.display_img_data('source image')
    filtered_img.display_img_data('prewitt_x output')



@pytest.mark.edge_detection_without_preprocessing
def test_prewitt_operator_without_preprocessing():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    kernel_prewitt_x = np.matrix([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
    prewitt_x_output = cv2.filter2D(gray_img_test.get_data(), -1, kernel_prewitt_x)

    kernel_prewitt_y = np.matrix([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
    prewitt_y_output = cv2.filter2D(gray_img_test.get_data(), -1, kernel_prewitt_y)

    prewitt_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\edge_detection_output\\prewitt_output"

    current_path = os.getcwd()


    norme_prewitt = calc_gradient_norm2(prewitt_x_output, prewitt_y_output)

    plt.figure(1)
    plt.title("prewitt_x_output")
    plt.imshow(prewitt_x_output, cmap="gray")
    os.chdir(prewitt_output_path)
    plt.savefig("prewitt_x_output")
    os.chdir(current_path)

    plt.figure(2)
    plt.title("prewitt_y_output")
    plt.imshow(prewitt_y_output, cmap="gray")
    os.chdir(prewitt_output_path)
    plt.savefig("prewitt_y_output")
    os.chdir(current_path)

    plt.figure(3)
    plt.title("norme 2 de Prewitt")
    plt.imshow(norme_prewitt, cmap="gray")
    os.chdir(prewitt_output_path)
    plt.savefig("norme 2 de Prewitt")
    os.chdir(current_path)
    plt.show()


@pytest.mark.edge_detection_with_preprocessing
def test_prewitt_operator_with_low_filtering():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    kernel_prewitt_x = np.matrix([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
    kernel_prewitt_y = np.matrix([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])

    prewitt_output_with_preprocessing_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\edge_detection_output\\prewitt_with_preprocessing"

    current_path = os.getcwd()
    i:int = 0
    for order in [3, 5, 7, 9, 11, 13, 15, 17, 19, 21]:
        average_kernel = float(1.0 / (order * order)) * np.ones([order, order])
        gray_img_data_filtered = cv2.filter2D(gray_img_test.get_data(), -1,average_kernel)

        prewitt_x_output = cv2.filter2D(gray_img_data_filtered, -1, kernel_prewitt_x)
        prewitt_y_output = cv2.filter2D(gray_img_data_filtered, -1, kernel_prewitt_y)

        norme_prewitt = calc_gradient_norm2(prewitt_x_output, prewitt_y_output)

        i += 1  # counter incrementation
        plt.figure(i)
        plt.title("norme 2 de Prewitt et filtre moyen d'ordre {}".format(order))
        plt.imshow(norme_prewitt, cmap="gray")
        os.chdir(prewitt_output_with_preprocessing_path)
        plt.savefig("{}-norme 2 de Prewitt et filtre moyen d'ordre {}".format(i,order))
        os.chdir(current_path)
    plt.show()


@pytest.mark.edge_detection_without_preprocessing
def test_sobel_operator_without_preprocessing():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    kernel_sobel_x = np.matrix([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
    kernel_sobel_y = np.matrix([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])

    sobel_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\edge_detection_output\\sobel_output"
    current_path = os.getcwd()

    sobel_x_output = cv2.filter2D(gray_img_test.get_data(), -1, kernel_sobel_x)
    sobel_y_output = cv2.filter2D(gray_img_test.get_data(), -1, kernel_sobel_y)

    norme_sobel = calc_gradient_norm2(sobel_x_output, sobel_y_output)

    plt.figure(1)
    plt.title("sobel_x_output")
    plt.imshow(sobel_x_output, cmap="gray")
    os.chdir(sobel_output_path)
    plt.savefig("sobel_x_output")
    os.chdir(current_path)

    plt.figure(2)
    plt.title("sobel_y_output")
    plt.imshow(sobel_y_output, cmap="gray")
    os.chdir(sobel_output_path)
    plt.savefig("sobel_y_output")
    os.chdir(current_path)

    plt.figure(3)
    plt.title("norme 2 de Sobel")
    plt.imshow(norme_sobel, cmap="gray")
    os.chdir(sobel_output_path)
    plt.savefig("norme 2 de sobel")
    os.chdir(current_path)
    plt.show()

@pytest.mark.edge_detection_with_preprocessing
def test_sobel_operator_with_low_filtering():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    kernel_sobel_x = np.matrix([[1, 0, -1], [2, 0, -2], [1, 0, -1]])
    kernel_sobel_y = np.matrix([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])

    sobel_output_with_preprocessing_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\edge_detection_output\\sobel_with_preprocessing"

    current_path = os.getcwd()
    i:int = 0
    for order in [3, 5, 7, 9, 11, 13, 15, 17, 19, 21]:
        average_kernel = float(1.0 / (order * order)) * np.ones([order, order])
        gray_img_data_filtered = cv2.filter2D(gray_img_test.get_data(), -1,average_kernel)

        sobel_x_output = cv2.filter2D(gray_img_data_filtered, -1, kernel_sobel_x)
        sobel_y_output = cv2.filter2D(gray_img_data_filtered, -1, kernel_sobel_y)

        norme_sobel = calc_gradient_norm2(sobel_x_output, sobel_y_output)

        i += 1  # counter incrementation
        plt.figure(i)
        plt.title("norme 2 de Sobel et filtre moyen d'ordre {}".format(order))
        plt.imshow(norme_sobel, cmap="gray")
        os.chdir(sobel_output_with_preprocessing_path)
        plt.savefig("{}-norme 2 de Sobel et filtre moyen d'ordre {}".format(i,order))
        os.chdir(current_path)
    plt.show()


def test_calc_histogram():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')
    histo_test = calcul_histogram(gray_img_test.get_data(), True, 'TotoImage histogram')


@pytest.mark.edge_detection_with_thresholding
def test_binary_threshold_prewitt():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    # kernel for 1st derivative along x and along y
    kernel_prewitt_x = np.matrix([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]])
    kernel_prewitt_y = np.matrix([[-1, -1, -1], [0, 0, 0], [1, 1, 1]])
    # prewitt data
    prewitt_x_output = cv2.filter2D(gray_img_test.get_data(), -1, kernel_prewitt_x)
    prewitt_y_output = cv2.filter2D(gray_img_test.get_data(), -1, kernel_prewitt_y)

    prewitt_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\edge_detection_output\\prewitt_with_binary_threshold"
    current_path = os.getcwd()

    # norme 2 for the gradient from Prewitt
    norme_prewitt = calc_gradient_norm2(prewitt_x_output, prewitt_y_output)

    for threshold in [10,20,30,40]:
        norme_prewitt_thresholded = binary_threshold(norme_prewitt, threshold)

        plt.figure()
        plt.subplot(2,2,2)
        plt.title('module of prewitt gradient')
        plt.hist(norme_prewitt.ravel(), 256, [0, 256])

        plt.subplot(2, 2, 4)
        plt.title('histogram module of prewitt gradient with binary threshold at {}'.format(threshold))
        plt.hist(norme_prewitt_thresholded.ravel(), 256, [0, 256])


        plt.subplot(2, 2, 1)
        plt.title('module of prewitt gradient')
        plt.imshow(norme_prewitt, cmap="gray")

        plt.subplot(2, 2, 3)
        plt.title('module of prewitt gradient with binary threshold at {}'.format(threshold))
        plt.imshow(norme_prewitt_thresholded, cmap="gray")

        os.chdir(prewitt_output_path)
        plt.savefig('module of prewitt gradient with binary threshold at {}'.format(threshold))
        os.chdir(current_path)

    plt.show()


@pytest.mark.edge_detection_with_thresholding
def test_hysterisis_threshold():
    gray_img_test = GrayImage(720, 1280)
    gray_img_test.set_data_from_file("C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_input",
                                     'TotoImage.jpg')

    threshold_high = 20
    threshold_low = 10
    data_out = hysterisis_threshold(gray_img_test, "prewitt", "No_low_pass", 5, threshold_high, threshold_low)

    hysterisis_threshold_output_path = "C:\\Users\\Adrien\\PycharmProjects\\ImageSW_PYTHON\\images_test_output\\edge_detection_output\prewitt_with_hysterisis_threshold"
    current_path = os.getcwd()

    plt.figure()
    plt.title('hysterisis threshold output')
    plt.imshow(data_out, cmap="gray")
    os.chdir(hysterisis_threshold_output_path)
    plt.savefig('module of prewitt gradient with hysterisis threshold at ({}, {})'.format(threshold_low, threshold_high))
    os.chdir(current_path)
    plt.show()