from GrayImage import*
import math
import numpy as np
from matplotlib import pyplot as plt



"""
mirror an image along a vertical or horizontal axis
@ param: gray_img, input GrayImage object
@ param: direction, direction for mirror effect
"""
def mirror_image(gray_img: GrayImage, direction: int):

    data_in = gray_img.get_data()

    # empty data_out matrix with data dimensions
    (Height,Width) = gray_img.get_data().shape
    data_out = np.zeros([Height, Width])
    #print("dimenstions of data_out matrix are {}".format(data_out.shape))
    #print("\ndimensions of the img data are {}".format(img.get_data().shape))

    for y in range(Height):
        for x in range(Width):

            if direction == 1: # mirror vertically

                data_out[y, x] = data_in[y, Width - 1 - x]

            elif direction == 2: # mirror horizontally

                data_out[y, x] = data_in[Height - 1 - y, x]

    return data_out



"""
rotates an image by 90°
@ param: img, input Image object
@ param: direction, direction for rotation 
"""
def rotation(gray_img: GrayImage, direction: int):

    data_in = gray_img.get_data()
    (Height, Width) = data_in.shape
    data_out = np.zeros([Width, Height])

    for y in range(Width):
        for x in range(Height):

            if direction == 1: # rotate right
                data_out[y, x] = data_in[Height - 1 - x, y]

            elif direction == -1: # rotate left
                data_out[y, x] = data_in[x, Width - 1 - y]

    return data_out


"""
rotates an image by an angle theta
@ param: gray_img, input GrayImage object
@ param: theta, angle of the rotation in degrees 
@ param: order, order of the interpolation
"""

def theta_rotation(gray_img: GrayImage, theta: float, order: int):
    theta = math.radians(theta)

    data_in = gray_img.get_data()
    (Height,Width) = data_in.shape
    data_out = np.zeros([Height, Width])

    # center of rotation coordinates
    x_center = int((Width - 1)*0.5)
    y_center = int((Height - 1) * 0.5)

    # coordinates for computing
    x: float = 0
    y: float = 0

    # debug purpose,
    counter: int = 0

    for yp in range(Height):
        for xp in range(Width):

            # computation of the rotation
            x = x_center + (xp - x_center) * math.cos(-theta) + (yp - y_center) * math.sin(-theta)
            y = y_center - (xp - x_center) * math.sin(-theta) + (yp - y_center) * math.cos(-theta)

            if order == 0: # nearest neighbor approximation

                x_int: int = int(x + 0.5)
                y_int: int = int(y + 0.5)

                # xp and yp implie the computation of x and y
                if (x_int < (Width) and x >= 0) and (y_int < (Height) and y >= 0):

                    data_out[yp, xp] = data_in[y_int, x_int]

            if order == 1: # Lagrange bilinear interpolation with unit square

                x_0: int = int(math.floor(x))
                x_1: int = int(math.ceil(x))
                y_0: int = int(math.floor(y))
                y_1: int = int(math.ceil(y))

                if (x_1 < Width and x_0 >= 0) and (y_1 < Height and y_0 >= 0):
                    f_00: int = data_in[y_0, x_0]

                    f_10: int = data_in[y_0, x_1]

                    f_01: int = data_in[y_1, x_0]

                    f_11: int = data_in[y_1, x_1]

                    # normalisation of x and y using f_OO value to avoid overflow with uint8 range
                    eps_x = x - x_0
                    eps_y = y - y_0

                    #f_xy: int = int(f_00 * (1 - x) * (1 - y) + f_10 * x * (1 - y) + f_01 * (1 - x) * y + f_11 * x * y)
                    f_xy: int = int(f_00 * (1 - eps_x) * (1 - eps_y) + f_10 * eps_x * (1 - eps_y) + f_01 * (1 - eps_x) * eps_y + f_11 * eps_x * eps_y)

                    data_out[yp, xp] = f_xy

            counter +=1
            #print("the current iteration is {}".format(counter))
    return data_out



