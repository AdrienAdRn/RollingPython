from Image import*
import math
import numpy as np
from matplotlib import pyplot as plt



"""
mirror an image along a vertical or horizontal axis
@ param: img, input Image object
@ param: direction, direction for mirror effect
"""
def BGR_mirror_image(img: Image, direction: int):

    data_in = img.get_data()

    # empty data_out matrix with data dimensions
    #data_out = np.zeros((3, img.get_height(), img.get_width()))

    data_out = np.zeros((img.get_height(), img.get_width(), 3))
    #print("dimenstions of data_out matrix are {}".format(data_out.shape))
    #print("\ndimensions of the img data are {}".format(img.get_data().shape))

    for y in range(img.get_height()):
        for x in range(img.get_width()):
            # assign B R and G values of pixels
            # dimensions are height, width, dimension

            if direction == 1: # mirror vertically

                data_out[y, x, 0] = data_in[y, img.get_width() - 1 - x, 0]
                data_out[y, x, 1] = data_in[y, img.get_width() - 1 - x, 1]
                data_out[y, x, 2] = data_in[y, img.get_width() - 1 - x, 2]

            elif direction == -1: # mirror horizontally

                data_out[y, x, 0] = data_in[img.get_height() - 1 - y, x, 0]
                data_out[y, x, 1] = data_in[img.get_height() - 1 - y, x, 1]
                data_out[y, x, 2] = data_in[img.get_height() - 1 - y, x, 2]

    #print("\nThe matrix data_out is equal to \n\n{}".format(data_out))
    #print("dimenstions of data_out matrix are {}".format(data_out.shape))

    img.set_data(data_out)
    data_in = None
    data_out = None


"""
rotates an image by 90°
@ param: img, input Image object
@ param: direction, direction for rotation 
"""
def BGR_rotation(img: Image, direction: int):

    data_in = img.get_data()
    data_out = np.zeros((img.get_width(), img.get_height(), 3))

    for y in range(img.get_width()):
        for x in range(img.get_height()):
            # assign B R and G values of pixels
            # dimensions are height, width, dimension

            if direction == 1: # rotate right
                data_out[y, x, 0] = data_in[img.get_height() - 1 - x, y, 0]
                data_out[y, x, 1] = data_in[img.get_height() - 1 - x, y, 1]
                data_out[y, x, 2] = data_in[img.get_height() - 1 - x, y, 2]

            elif direction == -1: # rotate left
                data_out[y, x, 0] = data_in[x, img.get_width() - 1 - y, 0]
                data_out[y, x, 1] = data_in[x, img.get_width() - 1 - y, 1]
                data_out[y, x, 2] = data_in[x, img.get_width() - 1 - y, 2]

    img.set_data(data_out)
    data_in = None
    data_out = None


"""
rotates an image by an angle theta
@ param: img, input Image object
@ param: theta, angle of the rotation in degrees 
@ param: order, order of the interpolation
"""

def BGR_theta_rotation(img: Image, theta: float, order: int):
    theta = math.radians(theta)

    data_in = img.get_data()
    data_out = np.zeros((img.get_height(), img.get_width(), 3))

    # center of rotation coordinates
    x_center = int((img.get_width() - 1)*0.5)
    y_center = int((img.get_height() - 1) * 0.5)

    # coordinates for computing
    x: float = 0
    y: float = 0

    # debug purpose,
    counter: int = 0

    for yp in range(img.get_height()):
        for xp in range(img.get_width()):

            # computation of the rotation
            x = x_center + (xp - x_center) * math.cos(-theta) + (yp - y_center) * math.sin(-theta)
            y = y_center - (xp - x_center) * math.sin(-theta) + (yp - y_center) * math.cos(-theta)

            if order == 0: # nearest neighbor approximation

                x_int: int = int(x + 0.5)
                y_int: int = int(y + 0.5)

                # xp and yp implies the computation of x and y
                if (x_int < (img.get_width()) and x >= 0) and (y_int < (img.get_height()) and y >= 0):

                    data_out[yp, xp, 0] = data_in[y_int, x_int, 0]
                    data_out[yp, xp, 1] = data_in[y_int, x_int, 1]
                    data_out[yp, xp, 2] = data_in[y_int, x_int, 2]

            if order == 1: # Lagrange bilinear interpolation with unit square

                x_0: int = int(math.floor(x))
                x_1: int = int(math.ceil(x))
                y_0: int = int(math.floor(y))
                y_1: int = int(math.ceil(y))

                if (x_1 < img.get_width() and x_0 >= 0) and (y_1 < img.get_height() and y_0 >= 0):
                    # img_out.data.setRGB(xp, yp, this.data.getRGB(x_int, y_int));
                    f_00_b: int = data_in[y_0, x_0, 0]
                    f_00_r: int = data_in[y_0, x_0, 1]
                    f_00_g: int = data_in[y_0, x_0, 2]

                    f_10_b: int = data_in[y_0, x_1,0]
                    f_10_g: int = data_in[y_0, x_1,1]
                    f_10_r: int = data_in[y_0, x_1,2]

                    f_01_b: int = data_in[y_1, x_0,0]
                    f_01_g: int = data_in[y_1, x_0,1]
                    f_01_r: int = data_in[y_1, x_0,2]

                    f_11_b: int = data_in[y_1, x_1,0]
                    f_11_g: int = data_in[y_1, x_1,1]
                    f_11_r: int = data_in[y_1, x_1,2]

                    f_xy_b: int = int(f_00_b * (1 - x) * (1 - y) + f_10_b * x * (1 - y) + f_01_b * (1 - x) * y + f_11_b * x * y)
                    f_xy_g: int = int(f_00_g * (1 - x) * (1 - y) + f_10_g * x * (1 - y) + f_01_g * (1 - x) * y + f_11_g * x * y)
                    f_xy_r: int = int(f_00_r * (1 - x) * (1 - y) + f_10_r * x * (1 - y) + f_01_r * (1 - x) * y + f_11_r * x * y)

                    # data_out[(xp, yp, f_xy);
                    data_out[yp, xp, 0] = f_xy_b
                    data_out[yp, xp, 1] = f_xy_g
                    data_out[yp, xp, 2] = f_xy_r




            counter +=1
            print("the current iteraation is {}".format(counter))
    img.set_data(data_out)

data_out = None
data_in = None
x = None
y = None
x_p = None
y_p = None
x_int = None
y_int = None

