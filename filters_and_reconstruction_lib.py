from GrayImage import*
import cv2
from skimage.exposure import rescale_intensity
import math
import numpy as np
from matplotlib import pyplot as plt


"""
def average_blur_cv2(gray_img: GrayImage, order: int):
    if(order in [3, 5, 7, 9, 11, 13]):
        average_kernel = float(1.0/(order*order))*np.ones([order, order])
        data_out = cv2.filter2D(gray_img, -1, average_kernel) # ddpeth -1 stands for dim data_out = dim input img
        return data_out
    else:
        breakpoint()
"""


def convolve(gray_img: GrayImage, kernel):
    (iH, iW) = gray_img.get_data().shape
    kernel_width = kernel.shape[1]

    # float32 image to allow values away from the range (0,255) during the computation
    output = np.zeros((iH, iW), dtype = "float32")

    # padding in order to keep the dimensions of the output image the same as input
    pad = int((kernel_width - 1)/2)

    gray_img_padded = cv2.copyMakeBorder(gray_img.get_data(), pad, pad, pad, pad, cv2.BORDER_REPLICATE)

    for y in np.arange(pad, iH + pad):
        for x in np.arange(pad, iW + pad):

            # neigborhood around the (x,y) coordinates
            roi = gray_img_padded[y - pad: y + pad + 1, x - pad: x + pad + 1]

            #convolution
            k = (roi*kernel).sum()

            output[y - pad, x - pad] = k
    # rescale values to [0,255] uint8
    output = rescale_intensity(output, in_range = (0,255))

    output = (output*255).astype("uint8")
    return output




def average_blur(gray_img: GrayImage, order: int):
    if(order in [3, 5, 7, 9, 11, 13, 15, 17, 19, 21]):
        average_kernel = float(1.0/(order*order))*np.ones([order, order])
        #data_out = convolve(gray_img, average_kernel)
        data_out = cv2.filter2D(gray_img.get_data(), -1, average_kernel)  # ddpeth -1 stands for dim data_out = dim input img
        return data_out
    else:
        breakpoint


"""
median filter processing with neighborood of size n
@param gray_img, GrayImage object, data attribute is a grayscale image
@param n, int, size of the neighborhood
"""
def median_filter(gray_img: GrayImage, n: int):
    # exctract image height and image width
    (iH, iW) = gray_img.get_data().shape

    # float32 image to allow values away from the range (0,255) during the computation
    output = np.zeros((iH, iW), dtype="float32")

    # padding in order to keep the dimensions of the output image the same as input
    pad = int((n - 1) / 2)

    gray_img_padded = cv2.copyMakeBorder(gray_img.get_data(), pad, pad, pad, pad, cv2.BORDER_REPLICATE)

    for y in np.arange(pad, iH + pad):
        for x in np.arange(pad, iW + pad):

            # neigborhood around the (x,y) coordinate
            roi = gray_img_padded[y - pad: y + pad + 1, x - pad: x + pad + 1]

            #aasign median of the roi
            output[y - pad, x - pad] = np.median(roi)
    # rescale values to [0,255] uint8
    output = rescale_intensity(output, in_range = (0,255))

    output = (output*255).astype("uint8")

    return output


"""
finite approximate of a gaussian filter kernel
@param gray_img, GrayImage object, data attribute is a grayscale image
@param n, int, size of the neighborhood
"""
def bilinear_filter(gray_img: GrayImage, n: int):

    if(n in [3,5]):

        # Pascal triangle
        pascal_0 = 1
        pascal_1 = np.matrix([1, 1])
        pascal_2 = np.matrix([1, 2, 1])
        pascal_3 = np.matrix([1, 3, 3, 1])
        pascal_4 = np.matrix([1, 4, 6, 4, 1])
        pascal_5 = np.matrix([1, 5, 10, 10, 5, 1])

        if(n == 3):
            divisor = (np.transpose(pascal_2)*pascal_2).sum()
            bilinear_kernel = np.transpose(pascal_2)*pascal_2* (1.0/float(divisor))


        elif(n == 5):
            bilinear_kernel = np.transpose(pascal_4)*pascal_4 * (1.0/( np.transpose(pascal_4)*pascal_4).sum())

        output_data = cv2.filter2D(gray_img.get_data(), -1, bilinear_kernel)
        return output_data




"""
apply the prewitt_x operator to find variations along x in the image
@param gray_img : the GrayImage object on which we apply the filter
"""
def prewitt_x(gray_img: GrayImage):

    kernel_prewitt_x = np.matrix([[-1, 0, 1],[-1, 0, 1],[-1, 0, 1]], dtype="float32")
    prewitt_x_output = cv2.filter2D(gray_img.get_data(), -1, kernel_prewitt_x)
    return prewitt_x_output


"""
TODO
@TODO
"""
def prewitt_y(gray_img: GrayImage):

    kernel_prewitt_y = np.matrix([[-1, -1, -1], [0, 0, 0], [1, 1, 1]], dtype="float32")
    prewitt_y_output = cv2.filter2D(gray_img.get_data(), -1, kernel_prewitt_y)
    return prewitt_y_output






"""
TODO
@TODO
"""
def sobel_y(gray_img: GrayImage):

    kernel_sobel_y = np.matrix([[-1, -2, -1], [0, 0, 0], [1, 2, 1]], dtype="float32")
    sobel_y_output = cv2.filter2D(gray_img.get_data(), -1, kernel_sobel_y)
    return sobel_y_output



"""
TODO
@TODO
"""
def sobel_x(gray_img: GrayImage):

    kernel_sobel_x = np.matrix([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]], dtype="float32")
    sobel_x_output = cv2.filter2D(gray_img.get_data(), -1, kernel_sobel_x)
    return sobel_x_output



"""
TODO
@TODO
"""
def calc_gradient_norm2(prewitt_x_output,prewitt_y_output):

    prewitt_height = prewitt_x_output.shape[0]
    prewitt_width = prewitt_x_output.shape[1]

    # initialize module matrix with float32 variables to prevent overflow
    norme_prewitt = np.zeros([prewitt_height, prewitt_width],dtype="float32")

    for y in range(prewitt_height - 1):
        for x in range(prewitt_width - 1):
            P_x = float(prewitt_x_output[y, x]) * float(prewitt_x_output[y, x])
            P_y = float(prewitt_y_output[y, x]) * float(prewitt_y_output[y, x])

            norme_prewitt[y, x] = math.sqrt(P_x + P_y)

    # rescale the output matrix to fit uint8 range
    norme_prewitt = rescale_intensity(norme_prewitt, in_range=(0, 255))
    norme_prewitt = (norme_prewitt * 255).astype("uint8")
    return norme_prewitt


"""
TODO
@TODO
"""
def gradient_direction(gradient_angle_index: float):
    step_angle: float = 2*math.pi*9
    y: int = 0
    x: int = 0

    if(0 <= gradient_angle_index < step_angle):
        (y, x) = (0, 1)

    elif(step_angle <= gradient_angle_index < 2*step_angle):
        (y, x) = (1, 1)

    elif (2*step_angle <= gradient_angle_index < 3*step_angle):
        (y, x) = (1, 0)

    elif (3*step_angle <= gradient_angle_index < 4*step_angle):
        (y, x) = (1, -1)

    elif (4*step_angle <= gradient_angle_index < 5*step_angle):
        (y, x) = (0, -1)

    elif (5*step_angle <= gradient_angle_index < 6*step_angle):
        (y, x) = (-1, -1)

    elif (6*step_angle <= gradient_angle_index < 7*step_angle):
        (y, x) = (-1, 0)

    elif (7*step_angle <= gradient_angle_index < 8*step_angle):
        (y, x) = (-1, 1)
    return (y, x)




"""
function to compute and possibly plot the histogram of a grayscale img data
@ gray_img_data : data of a grayscale image
@ to_plot : boolean to chose whether or not plot the histogram  
"""
def calcul_histogram(gray_img_data, to_plot: bool,plot_title:str):

    hist = cv2.calcHist(gray_img_data, [0], None, [256], [0, 256]) # img must be grayscale
    if(to_plot):
        plt.hist(gray_img_data.ravel(), 256, [0, 256]);
        plt.title(plot_title)
        plt.xlabel('pixel intensity')
        plt.ylabel('population [pixel]')
        plt.show()

    return hist

"""
Compute the output from a binary threshold on a norm2 gradient data image
@norm2_gradient_data : eucledian norm of the gradient of an image
@threshold_high : threshold for the method
"""
def binary_threshold(norm2_gradient_data, threshold_high:int):
    """

    :param norm2_gradient_data:
    :param threshold_high:
    :return:
    """
    (Height,Width) = norm2_gradient_data.shape
    output = np.zeros([Height, Width])

    for y in range(Height):
        for x in range(Width):

            if(norm2_gradient_data[y,x] > threshold_high):
                output[y, x] = 255
            else:
                output[y, x] = 0

    return output



"""
TODO
@TODO
"""
def hysterisis_threshold(gray_img_test: GrayImage, gradient_method: str, low_pass_filter: str, order:int, threshold_high, threshold_low):

    #TODO -> vérifier quelle matrice est padded ou non, et les range avec padded ou non
    #TODO -> vérifier les overflow sur les int en plaçant des float32 et rescale_intensity


    #padded image
    data_in = gray_img_test.get_data()
    (Height_in, Width_in) = data_in.shape
    pad: int = order  # data padding
    data_in_padded = cv2.copyMakeBorder(data_in, pad, pad, pad, pad, cv2.BORDER_REPLICATE)

    img_in_padded: GrayImage = GrayImage(720, 1080)
    img_in_padded.set_data(data_in_padded)

    #low_pass_filter
    if(low_pass_filter == "average"):
        data_out = average_blur(img_in_padded, order)
        img_in_padded.set_data(data_out)

    elif(low_pass_filter == "bilinear"):
        data_out = bilinear_filter(img_in_padded, order)
        img_in_padded.set_data(data_out)

    elif(low_pass_filter == "median"):
        data_out = median_filter(img_in_padded, order)
        img_in_padded.set_data(data_out)

    #gradient dI_x, gradient dI_y
    if (gradient_method == "prewitt"):
            dI_x_padded = prewitt_x(img_in_padded)
            dI_y_padded = prewitt_y(img_in_padded)

    #gradient_angle
    gradient_angle_padded = np.zeros((data_in_padded.shape[0], data_in_padded.shape[1]), dtype="float32")
    for y in range(dI_y_padded.shape[0]):
        for x in range(dI_y_padded.shape[1]):
            gradient_angle_padded[y, x] = math.atan2(dI_y_padded[y, x], dI_x_padded[y, x])


    # gradient module
    gradient_module_padded = calc_gradient_norm2(dI_x_padded, dI_y_padded)
    data_high_thresholded_padded = np.zeros((gradient_module_padded.shape[0], gradient_module_padded.shape[1]))

    # threshold_high
    for y in np.arange(pad, Height_in + pad):
        for x in np.arange(pad, Width_in + pad):

            if(gradient_module_padded[y,x] > threshold_high):
                data_high_thresholded_padded[y, x] = 255

    #data_out = np.zeros(Height_in, Width_in)
    data_out = data_high_thresholded_padded[pad: Height_in + pad + 1, pad: Width_in + pad + 1]

    #gradient direction and threshold_low
    for y in range(Height_in):           #(pad, pad + Height_in + 1):
        for x in range(Width_in):        #(pad, pad + Width_in + 1):
            if(threshold_low< gradient_module_padded[y + pad, x +pad]< threshold_high):

                (a, b) = gradient_direction(gradient_angle_padded[y + pad, x + pad])
                if(data_high_thresholded_padded[y + pad + a, x + pad + b] == 255):
                    data_out[y, x] = 255

    #TODO rescale intensity -> prevent overflow of uint8 values
    return data_out




def SNR(ROI):
    """

    :param ROI: Region Of Interest
    :return: SNR = signal to noise ratio, detection criteria
    """

    (h, w) = np.shape(ROI)
    roi_mean: float = 0
    counter = 0
    for i in range(h):
        for j in range(w):
            roi_mean += ROI[i,j]
            counter +=1

    roi_mean /= counter

    roi_std








