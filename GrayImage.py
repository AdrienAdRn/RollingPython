import numpy as np
from Image import*
from matplotlib import pyplot as plt
import os
import cv2

class GrayImage:

    width: int
    height: int

    ## constructor with empty data matrix
    def __init__(self, height: int, width: int) -> object:
        self.height: int = height
        self.width: int = width
        self.data = np.empty([height,width])

    # overloaded constructor with data imported thanks to a file
    # @classmethod
    # def FromFilename(path):

    """
    getters
    """

    def get_height(self) -> int:
        return self.height

    def get_width(self) -> int:
        return self.width

    def get_data(self):
        return self.data

    """	
    setters
    """

    def set_height(self, new_height):
        self.height = new_height

    def set_width(self, new_width):
        self.width = new_width

    def set_data(self, new_data):
        self.data = new_data


    def set_data_from_file(self, path:str,img_name:str):
        current_path = os.getcwd()
        os.chdir(path)
        # read image in grayscale format color values
        self.data = cv2.imread(img_name, 0)
        os.chdir(current_path)


    def set_data_from_imageClass(self, img: Image):
        # conversion from cv2 cvtcolor uses the ITU-R 601-2 luma transform
        # wheights for R,G and B values are gray_value = 0.299 B + 0.587 G + 0.114 R
        self.set_data(cv2.cvtColor(img.get_data(), cv2.COLOR_BGR2GRAY))


    def display_img_data(self,title : str):
        cv2.imshow('title', self.get_data())
        # The OpenCV waitKey() function is a required keyboard binding
        # function after imwshow()
        cv2.waitKey(0)
        # destroy all windows command
        cv2.destroyAllWindows()


    """
    Function that allows to save the Grayimage data in a given path with specific
    output name 
    @param : img_file_name, name of the output image with the extension .jpg, .png ...
    @param : path, path of the output image data
    """
    def save_image(self, img_file_name : str, path: str):
        current_path: str = os.getcwd()
        os.chdir(path)
        cv2.imwrite(img_file_name, self.get_data())
        os.chdir(current_path)


    def mapp_to_uint8(self):
        self.data = self.data/255


